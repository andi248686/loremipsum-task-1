import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.List;

import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class Tests {

    private WebDriver driver;
/*У нас є кілька основних тестів. Давайте додамо ще кілька, із більш складною логікою та Xpath.
Завдання
Виконайте тести, описані у файлі Part2 вашого варіанта.
*/
    @BeforeTest
    public void profileSetUp() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
    }

    @BeforeMethod
    public void testsSetUp() {
        driver = new ChromeDriver();//екзаемпляр хром драйвера
        driver.manage().window().maximize();//браузер на весь екран
        driver.get("https://lipsum.com/");// сайт
    }


    @Test(priority = 1)
    public void checkWordAtFirstParagraph() {
        driver.findElement(xpath("//a[@href=\"http://ru.lipsum.com/\"]")).click();
        System.out.println("вибрано русіш");
        Assert.assertEquals("https://ru.lipsum.com/", driver.getCurrentUrl());
        System.out.println("порівняно адресний рядок");

        //варіант пшуку тексту 1
        String actualText = driver.findElement(By.xpath("//p[contains(text(),'рыба')]")).getText();//вибрав 1 абзац і вніс в змінну actualText
        assertTrue(actualText.contains("рыба"));// порівняв змінну з потрібним словом
        System.out.println("рыба String");

        //варіант пшуку тексту 2
        List<WebElement> actualTextList = driver.findElements(xpath("//p[contains(text(),'рыба')]"));//лист із тексту 1 абзац і вніс в змінну actualTextList
        for (WebElement webElement : actualTextList) {
            assertTrue(webElement.getText().contains("рыба"));
        }
        System.out.println("рыба List");

        //варіант пшуку тексту 3
        driver.getPageSource().contains("рыба");
        if (true) {
            System.out.println("слово Рыба є");
        } else System.out.println("слово Рыба немає");

    }

    @Test(priority = 2)
    public void checkTextOfBeginParagraph(){
        driver.findElement(xpath("//input[@type=\"submit\"]")).click();
        System.out.println("Натисніть Створити Lorem Ipsum");
        //p[contains(text(), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit')]
        String textLorem = driver.findElement(xpath("//div[@id = 'lipsum']/p[1]")).getText();
        assertTrue(textLorem.contains("Lorem ipsum dolor sit amet, consectetur adipiscing elit"));
        System.out.println("Text відповідає Lorem ipsum dolor sit amet, consectetur adipiscing elit");
    }

    @Test(priority = 4)
    public void VerifyTheCheckbox()
    {
        driver.findElement(xpath("//input[@name='start']")).click();
        driver.findElement(xpath("//input [@name='generate']")).click();
        String ElementText = driver.findElement(xpath("//div[@id = 'lipsum']/p[1]")).getText();
        Assert.assertFalse(ElementText.startsWith("Lorem ipsum"));
    }












    @AfterMethod
    public void tearDown() {
        driver.quit(); 
    }


}

